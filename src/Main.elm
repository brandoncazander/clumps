module Main exposing (..)

import Browser
import Html exposing (Html, a, button, div, input, span, text)
import Html.Attributes exposing (class, value)
import Html.Events exposing (onClick, onInput)
import Http


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, subscriptions = subscriptions, view = view }


type alias Model =
    { genome : String, start : Int, end : Int, substring : String }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model "ACTGACGT" 0 200 "", Cmd.none )


type Msg
    = ChangeGenome String
    | ChangeStart String
    | ChangeEnd String
    | Load
    | Loaded (Result Http.Error String)
    | Reset
    | MoveLeft
    | MoveRight


load : Cmd Msg
load =
    Http.get
        { url = "/ecoli.txt"
        , expect = Http.expectString Loaded
        }


search : Cmd Msg
search =
    Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeGenome newGenome ->
            ( { model | genome = newGenome }, Cmd.none )

        ChangeStart startString ->
            let
                start =
                    String.toInt startString
            in
            case start of
                Nothing ->
                    ( { model | start = 0 }, Cmd.none )

                Just i ->
                    ( { model | start = i }, Cmd.none )

        ChangeEnd endString ->
            let
                end =
                    String.toInt endString
            in
            case end of
                Nothing ->
                    ( { model | end = 200 }, Cmd.none )

                Just i ->
                    ( { model | end = i }, Cmd.none )

        Load ->
            ( model, load )

        Loaded result ->
            case result of
                Ok fullText ->
                    ( { model | genome = fullText }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        Reset ->
            init ()

        MoveLeft ->
            ( { model | start = model.start - 1, end = model.end - 1 }, Cmd.none )

        MoveRight ->
            ( { model | start = model.start + 1, end = model.end + 1 }, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ div [ class "tools" ]
            [ span [] [ text "Enter genome" ]
            , input [ class "entry", value model.genome, onInput ChangeGenome ] []
            , div [ class "range" ]
                [ input [ value (String.fromInt model.start), onInput ChangeStart ] []
                , span [] [ text ":" ]
                , input [ value (String.fromInt model.end), onInput ChangeEnd ] []
                ]
            , span [] [ text "OR" ]
            , button [ class "action", onClick Load ] [ text "E. coli" ]
            , button [ class "action", onClick Reset ] [ text "reset" ]
            ]
        , div [ class "genome" ]
            (showGenome model)
        ]


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


showGenome : Model -> List (Html Msg)
showGenome model =
    let
        start =
            max 0 model.start

        end =
            min model.end (model.start + 2000)

        truncated =
            String.slice start end model.genome

        pre =
            if start > 0 && start /= end && start < end then
                [ a [ class "more", onClick MoveLeft ] [ text "←" ] ]

            else
                []

        post =
            if String.length model.genome > end then
                [ a [ class "more", onClick MoveRight ] [ text "→" ] ]

            else
                []
    in
    pre ++ List.map (\x -> showNucleotide x) (String.toList truncated) ++ post


showNucleotide : Char -> Html msg
showNucleotide genome =
    div [ class (String.fromChar genome) ] [ text (String.fromChar genome) ]


matches : String -> String -> List Int
matches genome pattern =
    String.indices genome pattern
