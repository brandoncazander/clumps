#!/usr/bin/env make -f

dist/main.js: src/Main.elm
	elm make src/Main.elm --output=dist/main.js

watch: dist/main.js
	while true; do \
		inotifywait -e close_write -q src/Main.elm; \
		make; \
	done

clean:
	rm -rf dist

.PHONY: watch clean
